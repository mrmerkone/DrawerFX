package application;

import application.enums.Tool;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import org.mariuszgromada.math.mxparser.Argument;
import org.mariuszgromada.math.mxparser.Expression;
import org.mariuszgromada.math.mxparser.mXparser;

import static javafx.collections.FXCollections.observableArrayList;

public class UserEventController {

    /*
     * Application graphics context
     */
    public static GraphicsContext gc;

    /*
     * Drawing variables
     */
    double CANVAS_HEIGHT = 496;
    double CANVAS_WIDTH = 400;
    Double[] LINE_WIDTHS = new Double[]{1.0, 2.0, 3.0, 4.0, 5.0};

    /*
     * Drawing variables
     */
    private double canvasCenterX;
    private double canvasCenterY;
    private double x1, y1, x2, y2;

    /*
     * Canvases
     */
    @FXML
    private Canvas canvas;
    /*
     * ChoiceBoxes
     */
    @FXML
    private ChoiceBox<Double> lineWidthChoiceBox;
    /*
     * Labels
     */
    @FXML
    private Label timeoutLabel;
    @FXML
    private Label periodLabel;
    /*
     * Buttons
     */
    @FXML
    private Button clearButton;
    @FXML
    private Button drawButton;
    /*
     * Sliders
     */
    @FXML
    private Slider drawingSpeedSlider;
    @FXML
    private Slider periodSlider;
    /*
     * Textfields
     */
    @FXML
    private TextField xExprText;
    @FXML
    private TextField yExprText;
    /*
     * Colorpickers
     */
    @FXML
    private ColorPicker colorPicker;

    public UserEventController() {
        System.out.println("UserEventController loaded");
    }

    private static void sleep(long timeout) {
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void setSceneElementsParams() {
        canvasCenterX = canvas.getWidth() / 2;
        canvasCenterY = canvas.getHeight() / 2;
        colorPicker.setValue(Color.BLUE);
        gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, CANVAS_HEIGHT, CANVAS_WIDTH);
        gc.setStroke(Color.BLUE);
        gc.setLineWidth(LINE_WIDTHS[0]);
        lineWidthChoiceBox.setItems(observableArrayList(LINE_WIDTHS));
    }

    @FXML
    private void initialize() {

        setSceneElementsParams();

        drawingSpeedSlider.valueProperty().addListener((ov, old_val, new_val) -> {
            timeoutLabel.setText("Thread tick timeout: " + new_val.intValue() + "ms");
        });
        lineWidthChoiceBox.setOnAction((event) -> {
            gc.setLineWidth(lineWidthChoiceBox.getValue());
        });

        periodSlider.valueProperty().addListener((ov, old_val, new_val) -> {
            periodLabel.setText("t from 0 to " + new_val.intValue() + "pi");
        });

        clearButton.setOnAction((event) -> {
            gc.setFill(Color.WHITE);
            gc.fillRect(0, 0, CANVAS_HEIGHT, CANVAS_WIDTH);
            gc.save();
        });

        canvas.setOnMousePressed((MouseEvent) -> {
            if (MenuController.tool == Tool.rectangle) {
                System.out.println(Integer.toString((int) MouseEvent.getX()));
                x1 = MouseEvent.getX();
                System.out.println(Integer.toString((int) MouseEvent.getY()));
                y1 = MouseEvent.getY();
            }

            if (MenuController.tool == Tool.line) {
                x1 = MouseEvent.getX();
                y1 = MouseEvent.getY();
            }
            if (MenuController.tool == Tool.pencil) {
                x1 = MouseEvent.getX();
                y1 = MouseEvent.getY();
            }

        });

        canvas.setOnMouseReleased((MouseEvent) -> {
            if (MenuController.tool == Tool.rectangle) {
                x2 = MouseEvent.getX();
                y2 = MouseEvent.getY();
                gc.setFill(colorPicker.getValue());
                if ((x1 < x2) & (y1 < y2)) {
                    gc.fillRect(x1, y1, x2 - x1, y2 - y1); // из левого верхнего угла
                } else if ((x2 < x1) & (y2 < y1)) {
                    gc.fillRect(x2, y2, x1 - x2, y1 - y2); // из правого нижнего угла
                } else if ((x2 < x1) & (y1 < y2)) {
                    gc.fillRect(x2, y1, x1 - x2, y2 - y1); // из правого верхнего угла
                } else if ((x1 < x2) & (y2 < y1)) {
                    gc.fillRect(x1, y2, x2 - x1, y1 - y2); // из левого нижнего угла
                }
            }

            if (MenuController.tool == Tool.line) {
                x2 = MouseEvent.getX();
                y2 = MouseEvent.getY();
                gc.strokeLine(x1, y1, x2, y2);
            }

        });

        canvas.setOnMouseDragged((MouseDragEvent) -> {
            if (MenuController.tool == Tool.pencil) {
                gc.setStroke(colorPicker.getValue());
                gc.strokeLine(x1, y1, MouseDragEvent.getX(), MouseDragEvent.getY());
                x1 = MouseDragEvent.getX();
                y1 = MouseDragEvent.getY();
            }
        });

        drawButton.setOnAction((event) -> {
            Runnable polarLine = () -> {
                double t = 0;
                Argument n = new Argument("t", t);
                Expression xExpr = new Expression(xExprText.getText(), n);
                Expression yExpr = new Expression(yExprText.getText(), n);
                x1 = xExpr.calculate() + canvasCenterX;
                y1 = yExpr.calculate() + canvasCenterY;
                try {
                    while (t < periodSlider.getValue() * Math.PI) {
                        sleep((long) drawingSpeedSlider.getValue() / 5);
                        n.setArgumentValue(t += Math.PI / 1000);
                        x2 = xExpr.calculate() + canvasCenterX;
                        y2 = yExpr.calculate() + canvasCenterY;
                        gc.setStroke(colorPicker.getValue());
                        gc.strokeLine(x1, y1, x2, y2);
                        x1 = x2;
                        y1 = y2;
                    }
                } catch (Exception e) {
                    mXparser.consolePrintln("x: " + xExpr.getExpressionString() + " = " + xExpr.calculate());
                    mXparser.consolePrintln("y: " + yExpr.getExpressionString() + " = " + yExpr.calculate());
                    e.printStackTrace();
                }
                periodSlider.setDisable(false);
                drawButton.setDisable(false);
            };
            periodSlider.setDisable(true);
            drawButton.setDisable(true);
            new Thread(polarLine).start();
        });
    }
}

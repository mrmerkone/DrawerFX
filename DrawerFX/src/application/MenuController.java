package application;

import application.enums.*;
import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class MenuController {

	public static Tool tool;

	@FXML
	private MenuItem myAbout;

	@FXML
	private MenuItem myVersion;

	@FXML
	private MenuItem myLineTool;

	@FXML
	private MenuItem myRectangleTool;

	@FXML
	private MenuItem myPencilTool;

	public MenuController() {
		System.out.println("rootLayoutController loaded");
	}

	@FXML
	private void initialize() {
		myAbout.setOnAction((event) -> {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("About");
			alert.setHeaderText(null);
			alert.setContentText("Made by Alex");
			alert.showAndWait();

		});
		myVersion.setOnAction((event) -> {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Version");
			alert.setHeaderText(null);
			alert.setContentText("Current version is 0.0.1");
			alert.showAndWait();

		});

		myLineTool.setOnAction((event) -> {
			tool = Tool.line;
		});

		myRectangleTool.setOnAction((event) -> {
			tool = Tool.rectangle;
		});

		myPencilTool.setOnAction((event) -> {
			tool = Tool.pencil;
		});

	}

}